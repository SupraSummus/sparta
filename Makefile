.SUFFIXES:

all:
	make -C source/ $@

clean:
	make -C source/ $@
	rm -f $(SPARTA_INCLUDE_DIR)*.h $(SPARTA_LIB_INCLUDE_DIR)*.h $(BIN_DIR)sparta $(LIB_DIR)*.so

remake: clean all

test: all
	make -C tests/ $@

%.test: all
	make -C tests/ $@

ROOT = ./
include $(ROOT)variables.mk
