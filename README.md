Compilation
===========

	git submodule update --init --recursive
	make

Run
===

	make test

This command will run 'unit tests' for sparta interpreter and basic libraries.

Syntax
======

See file `syntax`
