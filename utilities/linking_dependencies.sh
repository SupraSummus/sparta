#!/bin/bash

required_symbols() {
	nm --undefined-only -P $1 | \
	grep " U" | \
	cut -f1 -d " " | \
	sort | \
	uniq | \
	tr '\n' ' ' | \
	sed -e 's/^ *//' -e 's/ *$//'
}

who_provides() {
	# make (a|b|c...)
	SEARCHING_FOR="( "`echo $1 | sed 's/ /\| /g'`")"
	
	nm --defined-only --extern-only -o $2 | \
	grep -E "$SEARCHING_FOR" | \
	cut -f1 -d ":" | \
	sort | \
	uniq | \
	tr '\n' ' ' | \
	sed -e 's/^ *//' -e 's/ *$//'
}

ALL_OBJECTS=`echo $2 | sed -e 's/^ *//' -e 's/ *$//'`
NEW_OBJECTS=`echo $1 | sed -e 's/^ *//' -e 's/ *$//'`
OBJECTS=

#echo "### ALL_OBJECTS ###"
#echo $ALL_OBJECTS

while [ "$OBJECTS" != "$NEW_OBJECTS" ]; do
	#echo "### OBJECTS vs NEW_OBJECTS ###"
	#echo $OBJECTS
	#echo $NEW_OBJECTS
	
	OBJECTS=$NEW_OBJECTS
	
	REQUIRED_SYMBOLS=`required_symbols "$OBJECTS"`
	
	#echo "### REQUIRED_SYMBOLS ###"
	#echo $REQUIRED_SYMBOLS
	
	DELTA_OBJECTS=`who_provides "$REQUIRED_SYMBOLS" "$ALL_OBJECTS"`
	
	#echo "### DELTA_OBJECTS ###"
	#echo $DELTA_OBJECTS
	
	NEW_OBJECTS=`echo "$DELTA_OBJECTS"" ""$OBJECTS" | sed -e 's/ \.\// /' -e 's/^\.\///' | tr " " "\n" | sort | uniq | tr "\n" " "`
done

echo $OBJECTS
