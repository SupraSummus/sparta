#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

/**
 * as for now execution environment contains only memory manager
 */
typedef struct _MemoryManager Environment;

#endif
