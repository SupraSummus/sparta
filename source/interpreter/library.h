#ifndef LIBRARY_H
#define LIBRARY_H

#include <sparta/execution_node.h>
#include <stddef.h>

typedef struct {
	size_t string_count;
	char** strings;
} LibraryInitializerConfiguration;

typedef ExecutionNode* (LibraryInitializer)(Environment*, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent);

extern LibraryInitializer initialize;

#endif
