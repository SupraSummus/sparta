#ifndef EXECUTOR_H
#define EXECUTOR_H

#include <sparta/vector.h>
#include <sparta/execution_node.h>

/**
 * executes chain of instructions with given root and parameter
 */
extern ExecutionNode* execute_forest(
	Environment*,
	Vector const* instructions,
	ExecutionNode* parameter,
	ExecutionNode* root,
	ExecutionNode const* parent
);

/**
 */
extern ExecutionNode* execution_node_block_new(
	Environment*,
	Vector const* instructions,
	ExecutionNode* root,
	ExecutionNode const* parent
);

#endif
