#include "loader.h"
#include "../utilities/debug.h"
#include <dlfcn.h>

ExecutionNode* loader_load(
	Environment* environment,
	char const* module_name,
	LibraryInitializerConfiguration* module_configuration,
	ExecutionNode const* parent
) {
	/* load module */
	spam("loading module %s", module_name);
	void* module = dlopen(module_name, RTLD_LAZY);
	if (module == NULL) {
		erro("unable to load module: %s", dlerror());
		return NULL;
	}
	
	/* build node */
	#pragma GCC diagnostic ignored "-Wpedantic"
	LibraryInitializer* initializer = dlsym(module, "initialize");
	#pragma GCC diagnostic pop
	
	const char* error_message = dlerror();
	if (error_message != NULL) {
		erro("dlsym error for module: %s", error_message);
		dlclose(module);
		return NULL;
	}
	if (initializer == NULL) {
		erro("null root initializer (%s)", module_name);
		dlclose(module);
		return NULL;
	}
	
	return initializer(environment, module_configuration, parent);
}
