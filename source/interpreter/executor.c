#include "executor.h"
#include "parser.h"
#include <string.h>
#include "../utilities/debug.h"
#include <stdlib.h>
#include "memory_manager/memory_manager.h"

/**
 * cantext data for block node (function)
 */
typedef struct _ExecutionNodeBlockData {
	ExecutionNode* root;
	Vector const* instructions;
} ExecutionNodeBlockData;

/**
 * callback
 * calls function (casted to ExecutionNodeBlockData) with parameter
 */
ExecutionNodeHandler execution_node_block_handler;

ExecutionNode* execute_forest(
	Environment* environment,
	Vector const* forest,
	ExecutionNode* parameter,
	ExecutionNode* root,
	ExecutionNode const* parent
) {
	ExecutionNode* node = NULL;
	
	for (size_t i = 0; i < vector_size(forest); i ++) {
		SyntaxNode* parsing_node = vector_get(forest, i);
		
		if (parsing_node->type == SYNTAX_NODE_TYPE_BLOCK) {
			/* block (function) */
			
			/* compute root */
			ExecutionNode* block_root = execute_forest(
				environment,
				parsing_node->data.block.context_nodes,
				parameter,
				root,
				NULL
			);
			
			/* build execution node */
			ExecutionNode* execution_node = execution_node_block_new(
				environment,
				parsing_node->data.block.body_nodes,
				block_root,
				NULL
			);
			execution_node_not_needed(environment, block_root, NULL);
			
			/* execute */
			if (node == NULL) {
				node = execution_node;
			} else {
				ExecutionNode* new_node = execution_node_call(
					environment,
					node, execution_node,
					NULL /* stack */
				);
				
				execution_node_not_needed(environment, node, NULL);
				node = new_node;
				
				if (execution_node != NULL) {
					execution_node_not_needed(environment, execution_node, NULL);
					execution_node = NULL;
				}
			}
			
		} else if (parsing_node->type == SYNTAX_NODE_TYPE_BRACKET) {
			/* bracket */
			
			ExecutionNode* execution_node = execute_forest(
				environment,
				parsing_node->data.bracket.nodes,
				parameter,
				root,
				NULL /* result is needed by stack */
			);
			
			/* execute */
			if (node == NULL) {
				node = execution_node;
			} else {
				ExecutionNode* new_node = execution_node_call(
					environment,
					node, execution_node,
					NULL /* stack */
				);
				
				execution_node_not_needed(environment, node, NULL);
				node = new_node;
				
				if (execution_node != NULL) {
					execution_node_not_needed(environment, execution_node, NULL);
					execution_node = NULL;
				}
			}
			
		} else if (parsing_node->type == SYNTAX_NODE_TYPE_SYMBOL) {
			/* symbol */
			
			ExecutionNode* execution_node = execution_node_symbol_new(
				environment,
				parsing_node->data.symbol.symbol,
				NULL /* result is needed by stack */
			);
			
			/* execute */
			if (node == NULL) {
				node = execution_node;
			} else {
				ExecutionNode* new_node = execution_node_call(
					environment,
					node, execution_node,
					NULL /* stack */
				);
				
				execution_node_not_needed(environment, node, NULL);
				node = new_node;
				
				if (execution_node != NULL) {
					execution_node_not_needed(environment, execution_node, NULL);
					execution_node = NULL;
				}
			}
			
		} else if (parsing_node->type == SYNTAX_NODE_TYPE_PARAMETER) {
			/* parameter */
			
			if (node == NULL) {
				execution_node_needed(environment, parameter, NULL);
				node = parameter;
			} else {
				ExecutionNode* new_node = execution_node_call(
					environment,
					node, parameter,
					NULL /* stack */
				);
				
				execution_node_not_needed(environment, node, NULL);
				node = new_node;
			}
			
		} else if (parsing_node->type == SYNTAX_NODE_TYPE_ROOT) {
			/* root */
			
			if (node == NULL) {
				execution_node_needed(environment, root, NULL);
				node = root;
			} else {
				ExecutionNode* new_node = execution_node_call(
					environment,
					node, root,
					NULL /* stack */
				);
				
				execution_node_not_needed(environment, node, NULL);
				node = new_node;
			}
			
		} else if (parsing_node->type == SYNTAX_NODE_TYPE_DISCARD) {
			/* clear */
			
			if (node != NULL) {
				execution_node_not_needed(environment, node, NULL);
				node = NULL;
			}
			
		} else {
			erro("executor: unexpected parsing node");
			return NULL;
		}
	}
	
	if (node != NULL) {
		execution_node_needed(environment, node, parent);
		execution_node_not_needed(environment, node, NULL);
	}
	return node;
}

ExecutionNode* execution_node_block_new(
	Environment* environment,
	Vector const* instructions,
	ExecutionNode* root,
	ExecutionNode const* parent
) {
	/* build execution node */
	ExecutionNode* execution_node = execution_node_new(
		environment,
		execution_node_block_handler, NULL, sizeof(ExecutionNodeBlockData),
		parent
	);
	ExecutionNodeBlockData* data = execution_node_get_data(execution_node);
	
	/* set data */
	data->root = root;
	execution_node_needed(environment, root, execution_node);
	data->instructions = instructions;
	memory_manager_needed(environment, instructions, execution_node);
	
	return execution_node;
}

ExecutionNode* execution_node_block_handler(MemoryManager* memory_manager, ExecutionNode* self, ExecutionNode* parameter, ExecutionNode const* parent){
	ExecutionNodeBlockData* block_data = execution_node_get_data(self);
	return execute_forest(
		memory_manager,
		block_data->instructions,
		parameter,
		block_data->root,
		parent
	);
}
