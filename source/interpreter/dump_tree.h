#ifndef DUMP_TREE_H
#define DUMP_TREE_H

#include "parser.h"
#include "../utilities/vector.h"

/**
 * Returned strings has to be freed() after use
 */
extern char* dump_tree(SyntaxNode const*);
extern char* dump_forest(Vector const*);

#endif
