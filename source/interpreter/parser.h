#ifndef PARSER_H
#define PARSER_H

#include <sparta/vector.h>
#include <sparta/environment.h>
#include <stddef.h>

typedef enum {
	SYNTAX_NODE_TYPE_BLOCK,
	SYNTAX_NODE_TYPE_BRACKET,
	SYNTAX_NODE_TYPE_SYMBOL,
	SYNTAX_NODE_TYPE_PARAMETER,
	SYNTAX_NODE_TYPE_ROOT,
	SYNTAX_NODE_TYPE_DISCARD
} SyntaxNodeType;

typedef struct {
	SyntaxNodeType type;
	union {
		struct {
			Vector* context_nodes;
			Vector* body_nodes;
		} block;
		struct {
			Vector* nodes;
		} bracket;
		struct {
			char* symbol;
		} symbol;
	} data;
} SyntaxNode;

extern Vector* parse(
	Environment*,
	char const*, size_t* consumed,
	void const* parent
);

#endif
