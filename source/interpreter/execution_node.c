#include "execution_node.h"
#include "memory_manager/memory_manager.h"
#include "../utilities/debug.h"
#include <string.h>
#include <stdlib.h>

/**
 * execution node - only type
 * 
 * handler is a function and data is custom format context data
 * if handler is null: the node is a symbol, data is a pointer to string
 */
struct _ExecutionNode {
	ExecutionNodeHandler* handler;
	ExecutionNodeFinalizer* finalizer;
	char data[];
};

/**
 * basic functions for handling nodes
 * ==================================
 */

MemoryManagerFreeFunction execution_node_finalizer;

ExecutionNode* execution_node_call(MemoryManager* memory_manager, ExecutionNode* node, ExecutionNode* argument, ExecutionNode const* owner) {
	if (node->handler == NULL) {
		warn("execution_node_call(): you are trying to call a symbol");
		return NULL;
	}
	return node->handler(memory_manager, node, argument, owner);
}

ExecutionNode* execution_node_new(
	MemoryManager* memory_manager,
	ExecutionNodeHandler* handler, ExecutionNodeFinalizer* finalizer, size_t data_size,
	ExecutionNode const* parent
) {
	ExecutionNode* node = memory_manager_malloc(
		memory_manager,
		sizeof(ExecutionNode) + data_size,
		parent,
		finalizer == NULL ? NULL : execution_node_finalizer
	);
	node->handler = handler;
	node->finalizer = finalizer;
	return node;
}

void execution_node_needed(MemoryManager* memory_manager, ExecutionNode const* node, ExecutionNode const* parent) {
	if (node == NULL) {
		return;
	}
	memory_manager_needed(memory_manager, node, parent);
}

void execution_node_not_needed(MemoryManager* memory_manager, ExecutionNode const* node, ExecutionNode const* parent) {
	if (node == NULL) {
		return;
	}
	memory_manager_not_needed(memory_manager, (void*)node, parent);
}

bool execution_node_is(ExecutionNode const* node, ExecutionNodeHandler* handler) {
	if (node == NULL) {
		return false;
	}
	
	return node->handler == handler;
}

void* execution_node_get_data(ExecutionNode const* node) {
	if (node == NULL) {
		return NULL;
	}
	
	return (char*)node + offsetof(ExecutionNode, data);
}

ExecutionNodeHandler* execution_node_get_handler(ExecutionNode const* node) {
	if (node == NULL) {
		warn("execution_node_get_handler(): called on NULL node");
		return NULL;
	}
	return node->handler;
}

void execution_node_finalizer(MemoryManager* memory_manager, void* void_execution_node) {
	ExecutionNode* execution_node = void_execution_node;
	if (execution_node->finalizer != NULL) {
		execution_node->finalizer(memory_manager, execution_node);
	}
}

/**
 * symbol-related functions
 * ========================
 */

ExecutionNode* execution_node_symbol_new(MemoryManager* memory_manager, char const* symbol, ExecutionNode const* parent){
	size_t length = strlen(symbol) + 1;
	ExecutionNode* node = execution_node_new(memory_manager, NULL, NULL, length, parent);
	memcpy(execution_node_get_data(node), symbol, length);
	return node;
}

bool execution_node_is_symbol(ExecutionNode const* node, char const* symbol) {
	if (!execution_node_is_any_symbol(node)) {
		return false;
	}
	
	return strcmp(execution_node_get_data(node), symbol) == 0;
}

bool execution_node_is_any_symbol(ExecutionNode const* node) {
	if (node == NULL) {
		return false;
	}
	
	return execution_node_get_handler(node) == NULL;
}

char const* execution_node_get_symbol(ExecutionNode const* node) {
	if (!execution_node_is_any_symbol(node)) {
		return NULL;
	}
	
	return execution_node_get_data(node);
}

/**
 * funnctions for handling nodes with single pointer as a data
 * ===========================================================
 * (just for convenience)
 */

ExecutionNode* execution_node_pointer_new(
	Environment* environment,
	ExecutionNodeHandler* handler, void* pointer, ExecutionNodeFinalizer* finalizer,
	ExecutionNode const* parent
) {
	ExecutionNode* node = execution_node_new(environment, handler, finalizer, sizeof(void*), parent);
	*(void**)execution_node_get_data(node) = pointer;
	if (memory_manager_is_traced(environment, pointer)) {
		memory_manager_needed(environment, pointer, node);
	}
	return node;
}

void* execution_node_pointer_get_data(ExecutionNode* node) {
	return *(void**)execution_node_get_data(node);
}
