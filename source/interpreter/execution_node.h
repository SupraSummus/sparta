#ifndef EXECUTION_NODE_H
#define EXECUTION_NODE_H

#include <stdbool.h>
#include <sparta/environment.h>
#include <stddef.h>

/**
 * execution node - only type existing in sparta language
 *
 * node can be function (that takes one parameter) or symbol
 */
typedef struct _ExecutionNode ExecutionNode;

/**
 * handler - function which takes execution node (argument of a call) and data (private context) and returns another node
 * handler is called when node is called
 */
typedef ExecutionNode* (ExecutionNodeHandler)(Environment*, ExecutionNode* self, ExecutionNode* parameter, ExecutionNode const* parent);

/**
 * function called when node is not needed anymore
 */
typedef void (ExecutionNodeFinalizer)(Environment*, ExecutionNode* node);

/**
 * basic functions for handling nodes
 * ==================================
 */

/**
 * construct new node with
 *   given handler
 *   private data of custom format
 *   function to free private data when node is no longer needed
 *
 * node is registered in memory manager with given parent
 *
 * finalizer is called when node is destructed
 */
extern ExecutionNode* execution_node_new(Environment*, ExecutionNodeHandler*, ExecutionNodeFinalizer*, size_t data_size, ExecutionNode const* parent);

/**
 * set dependencies
 */
extern void execution_node_needed(Environment*, ExecutionNode const* node, ExecutionNode const* parent);
extern void execution_node_not_needed(Environment*, ExecutionNode const* node, ExecutionNode const* parent);

/**
 * calls node with argument
 *
 * If node is symbol - result is (probably) segfault. So, don't call symbols.
 */
extern ExecutionNode* execution_node_call(Environment*, ExecutionNode* node, ExecutionNode* argument, ExecutionNode const* owner);

/**
 * access node data and handler
 */
extern bool execution_node_is(ExecutionNode const*, ExecutionNodeHandler*);
extern void* execution_node_get_data(ExecutionNode const*);
extern ExecutionNodeHandler* execution_node_get_handler(ExecutionNode const*);

/**
 * symbol-related functions
 * ========================
 */

/**
 * constructs new symbol
 */
extern ExecutionNode* execution_node_symbol_new(Environment*, char const*, ExecutionNode const* parent);

/**
 * access symbol data
 */
extern bool execution_node_is_symbol(ExecutionNode const*, char const*);
extern bool execution_node_is_any_symbol(ExecutionNode const*);
extern char const* execution_node_get_symbol(ExecutionNode const*);

/**
 * funnctions for handling nodes with single pointer as a data
 * ===========================================================
 * (just for convenience)
 */

/**
 * constructs node that wraps another node - for convinience
 */
extern ExecutionNode* execution_node_pointer_new(
	Environment*,
	ExecutionNodeHandler*, void* pointer, ExecutionNodeFinalizer*,
	ExecutionNode const* parent
);

/**
 * access pointer-node data
 */
extern void* execution_node_pointer_get_data(ExecutionNode*);

#endif
