#ifndef LOADER_H
#define LOADER_H

#include <sparta/execution_node.h>
#include <sparta/library.h>

extern ExecutionNode* loader_load(
	Environment*,
	char const* name,
	LibraryInitializerConfiguration*,
	ExecutionNode const* parent
);

// TODO unload modules

#endif
