#include "dump_tree.h"
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include "../utilities/strjoin.h"

char* _dump_tree(SyntaxNode const*, char const*, char const*, char const*);
char* _dump_forest(Vector const*, char const*, char const*, char const*);

char* dump_tree(SyntaxNode const* node) {
	return _dump_tree(node, "", "\n", "    ");
}

char* dump_forest(Vector const* vector) {
	return _dump_forest(vector, "", "\n", "    ");
}

char* _dump_tree(
	SyntaxNode const* node,
	char const* prefix,
	char const* sufix,
	char const* intendation
) {
	Vector* v = vector_new_standard();
	vector_push(v, strdup(prefix));
	vector_push(v, strdup(intendation));
	char* deeper_prefix = strjoin(v);
	
	v = vector_new_standard();
	
	if (node->type == SYNTAX_NODE_TYPE_BLOCK) {
		vector_push(v, strdup(prefix));
		vector_push(v, strdup("["));
		vector_push(v, strdup(sufix));
		
		vector_push(v, _dump_forest(node->data.block.context_nodes, deeper_prefix, sufix, intendation));
		
		vector_push(v, strdup(prefix));
		vector_push(v, strdup("] {"));
		vector_push(v, strdup(sufix));
		
		vector_push(v, _dump_forest(node->data.block.body_nodes, deeper_prefix, sufix, intendation));
		
		vector_push(v, strdup(prefix));
		vector_push(v, strdup("}"));
		vector_push(v, strdup(sufix));
	}
	
	if (node->type == SYNTAX_NODE_TYPE_BRACKET) {
		vector_push(v, strdup(prefix));
		vector_push(v, strdup("("));
		vector_push(v, strdup(sufix));
		
		vector_push(v, _dump_forest(node->data.bracket.nodes, deeper_prefix, sufix, intendation));
		
		vector_push(v, strdup(prefix));
		vector_push(v, strdup(")"));
		vector_push(v, strdup(sufix));
	}
	
	if (node->type == SYNTAX_NODE_TYPE_SYMBOL) {
		vector_push(v, strdup(prefix));
		vector_push(v, strdup(node->data.symbol.symbol));
		vector_push(v, strdup(sufix));
	}
	
	if (node->type == SYNTAX_NODE_TYPE_PARAMETER) {
		vector_push(v, strdup(prefix));
		vector_push(v, strdup("_"));
		vector_push(v, strdup(sufix));
	}
	
	if (node->type == SYNTAX_NODE_TYPE_ROOT) {
		vector_push(v, strdup(prefix));
		vector_push(v, strdup("@"));
		vector_push(v, strdup(sufix));
	}
	
	if (node->type == SYNTAX_NODE_TYPE_DISCARD) {
		vector_push(v, strdup(prefix));
		vector_push(v, strdup(";"));
		vector_push(v, strdup(sufix));
	}
	
	free(deeper_prefix);
	return strjoin(v);
}

char* _dump_forest(
	Vector const* nodes,
	char const* prefix,
	char const* sufix,
	char const* intendation
) {
	/* gather rendered strings */
	Vector* rendered = vector_new_standard();
	
	for (size_t i = 0; i < vector_size(nodes); i ++) {
		vector_push(rendered, _dump_tree(vector_get(nodes, i), prefix, sufix, intendation));
	}
	
	/* concatenate */
	return strjoin(rendered);
}
