#include "parser.h"
#include <stdlib.h>
#include <string.h>
#include "../utilities/debug.h"
#include "../utilities/strjoin.h"
#include "memory_manager/memory_manager.h"
#include <stdbool.h>
#include <ctype.h>

static Vector* parse_nodes(Environment*, char const*, size_t*, void const* parent);
static MemoryManagerFreeFunction _vector_finalizer;
static SyntaxNode* parse_node(Environment*, char const*, size_t*, void const* parent);
static SyntaxNode* parse_block(Environment*, char const*, size_t*, void const* parent);
static SyntaxNode* parse_bracket(Environment*, char const*, size_t*, void const* parent);
static SyntaxNode* parse_symbol(Environment*, char const*, size_t*, void const* parent);
static bool is_character_from_simple_symbol(char ch);
static MemoryManagerFreeFunction _symbol_node_finalizer;
static SyntaxNode* parse_parameter(Environment*, char const*, size_t*, void const* parent);
static SyntaxNode* parse_root(Environment*, char const*, size_t*, void const* parent);
static SyntaxNode* parse_discard(Environment*, char const*, size_t*, void const* parent);
static size_t parse_optional_spaces(char const*, size_t*);

/**
 * public
 * ======
 */

Vector* parse(
	Environment* environment,
	char const* string, size_t* consumed,
	void const* parent
) {
	if (consumed == NULL) {
		size_t consumed_data = 0;
		consumed = &consumed_data;
	}
	
	size_t string_length = strlen(string);
	Vector* forest = parse_nodes(environment, string, consumed, parent);
	
	/* warn if not all input was consumed */
	spam("parsed, consumed %i of %i", *consumed, string_length);
	if (*consumed != string_length) {
		warn("not all input was consumed (%zu characters of total %zu)", *consumed, string_length);
	}
	spam("forest length: %i", vector_size(forest));
	
	return forest;
}

/**
 * private
 * =======
 */

static Vector* parse_nodes(Environment* environment, char const* str, size_t* consumed, void const* parent) {
	Vector* nodes = memory_manager_malloc(environment, vector_sizeof(), parent, _vector_finalizer);
	vector_initialize_standard(nodes);
	
	while (true) {
		/* trim */
		parse_optional_spaces(str, consumed);
		
		/* node */
		SyntaxNode* node = parse_node(environment, str, consumed, nodes);
		if (node == NULL) {
			break;
		}
		vector_push(nodes, node);
	}
	
	/* trim */
	parse_optional_spaces(str, consumed);
	
	return nodes;
}

static void _vector_finalizer(MemoryManager* memory_manager, void* void_vector) {
	(void)memory_manager;
	Vector* vector = void_vector;
	vector_deinitialize(vector);
}

static SyntaxNode* parse_node(Environment* environment, char const* str, size_t* consumed, void const* parent) {
	SyntaxNode* node = NULL;
	
	/* try to parse block */
	if (node == NULL) {
		node = parse_block(environment, str, consumed, parent);
	}
	
	/* try to parse bracket */
	if (node == NULL) {
		node = parse_bracket(environment, str, consumed, parent);
	}
	
	/* try to parse symbol */
	if (node == NULL) {
		node = parse_symbol(environment, str, consumed, parent);
	}
	
	/* try to parse parameter */
	if (node == NULL) {
		node = parse_parameter(environment, str, consumed, parent);
	}
	
	/* try to parse root */
	if (node == NULL) {
		node = parse_root(environment, str, consumed, parent);
	}
	
	/* try to parse discard */
	if (node == NULL) {
		node = parse_discard(environment, str, consumed, parent);
	}
	
	return node;
}

static SyntaxNode* parse_block(Environment* environment, char const* str, size_t* consumed, void const* parent) {
	SyntaxNode* node = memory_manager_malloc(environment, sizeof(SyntaxNode), parent, NULL);
	node->type = SYNTAX_NODE_TYPE_BLOCK;
	node->data.block.context_nodes = NULL;
	node->data.block.body_nodes = NULL;
	
	size_t i = *consumed;
	
	/* [ */
	if (str[i] != '[') {
		/* "[" expected */
		memory_manager_not_needed(environment, node, parent);
		return NULL;
	}
	i ++;
	
	/* context nodes */
	node->data.block.context_nodes = parse_nodes(environment, str, &i, node);
	
	/* ] */
	if (str[i] != ']') {
		/* "]" expected */
		memory_manager_not_needed(environment, node, parent);
		return NULL;
	}
	i ++;
	
	/* trim */
	parse_optional_spaces(str, &i);
	
	/* { */
	if (str[i] != '{') {
		/* "{" expected */
		memory_manager_not_needed(environment, node, parent);
		return NULL;
	}
	i ++;
	
	/* body nodes */
	node->data.block.body_nodes = parse_nodes(environment, str, &i, node);
	
	/* } */
	if (str[i] != '}') {
		/* "}" expected */
		memory_manager_not_needed(environment, node, parent);
		return NULL;
	}
	i ++;
	
	*consumed = i;
	
	return node;
}

static SyntaxNode* parse_bracket(Environment* environment, char const* str, size_t* consumed, void const* parent) {
	SyntaxNode* node = memory_manager_malloc(environment, sizeof(SyntaxNode), parent, NULL);
	node->type = SYNTAX_NODE_TYPE_BRACKET;
	node->data.bracket.nodes = NULL;
	
	size_t i = *consumed;
	
	/* ( */
	if (str[i] != '(') {
		/* "(" expected */
		memory_manager_not_needed(environment, node, parent);
		return NULL;
	}
	i ++;
	
	/* nodes */
	node->data.bracket.nodes = parse_nodes(environment, str, &i, node);
	
	/* ) */
	if (str[i] != ')') {
		/* ")" expected */
		memory_manager_not_needed(environment, node, parent);
		return NULL;
	}
	i ++;
	
	*consumed = i;
	
	return node;
}

static SyntaxNode* parse_symbol(Environment* environment, char const* str, size_t* consumed, void const* parent) {
	char* symbol_string = NULL;
	size_t offset = 0;
	
	if (str[*consumed] == '"' || str[*consumed] == '\'') {
		/* quoted symbol */
		
		char quote = str[*consumed];
		
		Vector* v = vector_new_standard();
		
		/* omit opening " */
		offset ++; 
		
		while (true) {
			/* consume all 'normal' characters */
			size_t length = 0;
			while (
				str[*consumed + offset + length] != '\\' &&
				str[*consumed + offset + length] != quote
			) {
				length ++;
			}
			vector_push(v, strndup(str + *consumed + offset, length));
			offset += length;
			
			/* not-normal characters */
			if (str[*consumed + offset] == quote) {
				offset ++;
				break;
			} else if (str[*consumed + offset] == '\\') {
				offset ++;
				vector_push(v, strndup(str + *consumed + offset, 1));
				offset ++;
			} else {
				/* something unexpected */
				return NULL;
			}
		}
		
		symbol_string = strjoin(v);
		
	} else if (
		is_character_from_simple_symbol(str[*consumed + offset]) &&
		str[*consumed + offset] != '@' &&
		str[*consumed + offset] != '_'
	) {
		/* simple symbol */
		while (is_character_from_simple_symbol(str[*consumed + offset])) {
			offset ++;
		}
		
		symbol_string = strndup(str + *consumed, offset);
	} else {
		/* expected simple symbol or """ or "'" */
		return NULL;
	}
	
	SyntaxNode* node = memory_manager_malloc(environment, sizeof(SyntaxNode), parent, _symbol_node_finalizer);
	if (node == NULL) {
		return NULL;
	}
	node->type = SYNTAX_NODE_TYPE_SYMBOL;
	node->data.symbol.symbol = symbol_string;
	
	*consumed += offset;
	
	return node;
}

static bool is_character_from_simple_symbol(char ch) {
	return isgraph(ch) && /* graphical */
		ch != '[' && ch != ']' &&
		ch != '{' && ch != '}' &&
		ch != '(' && ch != ')' &&
		ch != '"' && ch != '\'' &&
		ch != ';';
}

static void _symbol_node_finalizer(MemoryManager* memory_manager, void* void_syntax_node) {
	(void)memory_manager;
	SyntaxNode* node = void_syntax_node;
	free(node->data.symbol.symbol);
}

static SyntaxNode* parse_parameter(Environment* environment, char const* str, size_t* consumed, void const* parent) {
	if (str[*consumed] == '_') {
		(*consumed) ++;
		SyntaxNode* node = memory_manager_malloc(environment, sizeof(SyntaxNode), parent, NULL);
		node->type = SYNTAX_NODE_TYPE_PARAMETER;
		
		return node;
	} else {
		return NULL;
	}
}
	
static SyntaxNode* parse_root(Environment* environment, char const* str, size_t* consumed, void const* parent) {
	if (str[*consumed] == '@') {
		(*consumed) ++;
		SyntaxNode* node = memory_manager_malloc(environment, sizeof(SyntaxNode), parent, NULL);
		node->type = SYNTAX_NODE_TYPE_ROOT;
		
		return node;
	} else {
		return NULL;
	}
}

static SyntaxNode* parse_discard(Environment* environment, char const* str, size_t* consumed, void const* parent) {
	if (str[*consumed] == ';') {
		(*consumed) ++;
		SyntaxNode* node = memory_manager_malloc(environment, sizeof(SyntaxNode), parent, NULL);
		node->type = SYNTAX_NODE_TYPE_DISCARD;
		
		return node;
	} else {
		return NULL;
	}
}

static size_t parse_optional_spaces(char const* str, size_t* consumed) {
	size_t n = 0;
	while (isspace(str[*consumed])) {
		(*consumed) ++;
		n ++;
	}
	return n;
}


