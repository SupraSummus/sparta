#include "executor.h"
#include "loader.h"
#include "parser.h"
#include "memory_manager/memory_manager.h"
#include "../utilities/debug.h"
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>

int main(int argc, char** argv) {
	spam("hello");
	
	char const* root_module_file_name = NULL;
	
	size_t original_string_count = 0;
	LibraryInitializerConfiguration* root_module_configuration = malloc(sizeof(LibraryInitializerConfiguration));
	root_module_configuration->strings = NULL;
	root_module_configuration->string_count = 0;
	
	char const* program_file_name = NULL;
	
	if (argc == 3) {
		root_module_file_name = argv[1];
		program_file_name = argv[2];
	} else if (argc > 3) {
		root_module_file_name = argv[1];
		original_string_count = argc - 3;
		root_module_configuration->string_count = original_string_count;
		root_module_configuration->strings = argv + 2;
		program_file_name = argv[argc - 1];
	} else {
		erro("usage: %s root_module_file [root_module_config ...] program_file", argv[0]);
		return EXIT_FAILURE;
	}
	
	/* read program file */
	FILE* program_file = fopen(program_file_name, "rb");
	if (program_file == NULL) {
		erro("unable to open program file %s: %s", program_file_name, strerror(errno));
		return EXIT_FAILURE;
	}
	fseek(program_file, 0, SEEK_END);
	size_t fsize = ftell(program_file);
	fseek(program_file, 0, SEEK_SET);
	
	char* input = malloc(fsize + 1);
	memset(input, 0, fsize + 1);
	fread(input, fsize, 1, program_file);
	
	fclose(program_file);
	
	/* new memory manager */
	MemoryManager* memory_manager = memory_manager_new();
	
	/* parse */
	Vector* forest = parse(memory_manager, input, NULL, NULL);
	
	/* build root */
	ExecutionNode* root = loader_load(memory_manager, root_module_file_name, root_module_configuration, NULL);
	
	/* check if whole configuration was used */
	if (root_module_configuration->string_count != 0) {
		warn(
			"not all root configuration was used (%zu of %zu strings)",
			original_string_count - root_module_configuration->string_count, original_string_count
		);
	}
	
	/* execute */
	ExecutionNode* result = execute_forest(
		memory_manager,
		forest, NULL, root,
		NULL
	);
	
	/* free*/
	if (result != NULL) {
		memory_manager_not_needed(memory_manager, result, NULL);
		result = NULL;
	}
	if (root != NULL) {
		memory_manager_not_needed(memory_manager, root, NULL);
		root = NULL;
	}
	memory_manager_not_needed(memory_manager, forest, NULL);
	memory_manager_free(memory_manager);
	memory_manager = NULL;
	free(root_module_configuration);
	free(input);
	
	spam("bye");
	return EXIT_SUCCESS;
}
