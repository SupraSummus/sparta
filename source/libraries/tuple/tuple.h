#ifndef LIBSPARTA_TUPLE_H
#define LIBSPARTA_TUPLE_H

#include <sparta/execution_node.h>
#include <sparta/library.h>

/**
 * <int64> -> <tuple_builder>, <tuple> (when arg is 0)
 */
extern ExecutionNodeHandler tuple_factory;

/**
 * *, <*> -> <tuple_builder>, <tuple>
 */
extern ExecutionNodeHandler tuple_builder;

/**
 * <int64> -> *, <*>
 */
extern ExecutionNodeHandler tuple;

/**
 * utils
 */
extern ExecutionNode* tuple_factory_new(Environment*, ExecutionNode const* parent);

#endif
