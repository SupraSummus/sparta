#include "tuple.h"
#include "../../utilities/debug.h"
#include <sparta/execution_node.h>
#include <sparta/library.h>
#include <sparta/lib/int64.h>
#include <stddef.h>

typedef struct {
	/**
	 * total size of a tuple
	 */
	size_t size;
	ExecutionNode* nodes[];
} TupleData;

typedef struct {
	/**
	 * number of elements this builder will accept
	 */
	size_t writes_remaining;
	
	/**
	 * pointer to preceding builder
	 */
	ExecutionNode* previous;
	
	/**
	 * node stored at this stage of builder
	 */
	ExecutionNode* data;
} TupleBuilderData;

static ExecutionNode* tuple_new_from_builder(Environment* memory_manager, ExecutionNode* builder, ExecutionNode const* parent);
static ExecutionNode* tuple_builder_new(
	Environment* memory_manager,
	size_t writes_remaining, ExecutionNode* previous, ExecutionNode* data,
	ExecutionNode const* parent
);

/**
 * private
 */

static ExecutionNode* tuple_new_from_builder(Environment* memory_manager, ExecutionNode* builder, ExecutionNode const* parent) {
	/* count elements */
	TupleBuilderData* tuple_builder_data = execution_node_get_data(builder);
	size_t element_count = 0;
	while (tuple_builder_data->previous != NULL) {
		tuple_builder_data = execution_node_get_data(tuple_builder_data->previous);
		element_count ++;
	}
	
	/* build node */
	ExecutionNode* tuple_node = execution_node_new(
		memory_manager,
		tuple, NULL, sizeof(TupleData) + sizeof(ExecutionNode*) * element_count,
		parent
	);
	TupleData* data = execution_node_get_data(tuple_node);
	data->size = element_count;
	
	/* rewrite */
	tuple_builder_data = execution_node_get_data(builder);
	for (size_t i = 0; i < data->size; i ++) {
		data->nodes[data->size - i - 1] = tuple_builder_data->data;
		execution_node_needed(memory_manager, tuple_builder_data->data, tuple_node);
		
		/* get next builder */
		tuple_builder_data = execution_node_get_data(tuple_builder_data->previous);
	}
	
	return tuple_node;
}

static ExecutionNode* tuple_builder_new(
	Environment* memory_manager,
	size_t writes_remaining, ExecutionNode* previous, ExecutionNode* data,
	ExecutionNode const* parent
) {
	/* build node */
	ExecutionNode* builder = execution_node_new(
		memory_manager,
		tuple_builder, NULL, sizeof(TupleBuilderData),
		parent
	);
	TupleBuilderData* builder_data = execution_node_get_data(builder);
	
	/* fill data */
	builder_data->writes_remaining = writes_remaining;
	
	execution_node_needed(memory_manager, previous, builder);
	builder_data->previous = previous;
	
	execution_node_needed(memory_manager, data, builder);
	builder_data->data = data;
	
	/* build tuple if all data is collected */
	if (builder_data->writes_remaining == 0) {
		ExecutionNode* tuple = tuple_new_from_builder(memory_manager, builder, parent);
		/* forget about current builder */
		execution_node_not_needed(memory_manager, builder, parent);
		return tuple;
	}
	
	return builder;
}

/**
 * initializer
 */

ExecutionNode* initialize(Environment* memory_manager, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	(void)configuration;
	return tuple_factory_new(memory_manager, parent);
}

/**
 * node handlers
 */

ExecutionNode* tuple_factory(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)self;
	
	if (execution_node_is(argument, int64) && int64_get_value(argument) >= 0) {
		return tuple_builder_new(memory_manager, int64_get_value(argument), NULL, NULL, parent);
	}
	
	warn("tuple_factory expected nonnegative int64");
	return NULL;
}

ExecutionNode* tuple_builder(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	TupleBuilderData* previous_builder_data = execution_node_get_data(self);
	
	return tuple_builder_new(
		memory_manager,
		previous_builder_data->writes_remaining - 1,
		self,
		argument,
		parent
	);
}

ExecutionNode* tuple(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	TupleData* data = execution_node_get_data(self);
	
	if (!execution_node_is(argument, int64)){
		warn("tuple expected int64");
		return NULL;
	}
	
	if (int64_get_value(argument) < 0 ) {
		warn("tuple expected positive index");
		return NULL;
	}
	
	if ((size_t)int64_get_value(argument) >= data->size ) {
		warn("tuple of size %zu got index %zu", data->size, (size_t)int64_get_value(argument));
		return NULL;
	}
	
	ExecutionNode* result = data->nodes[int64_get_value(argument)];
	execution_node_needed(memory_manager, result, parent);
	return result;
}

/**
 * utils
 */

ExecutionNode* tuple_factory_new(Environment* memory_manager, ExecutionNode const* parent) {
	return execution_node_new(
		memory_manager,
		tuple_factory, NULL, 0,
		parent
	);
}
