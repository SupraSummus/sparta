#include "ignore.h"
#include <sparta/library.h>
#include <sparta/execution_node.h>

ExecutionNode* initialize(Environment* memory_manager, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	(void)configuration;
	return ignore_new(memory_manager, parent);
}

ExecutionNode* ignore(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)argument;
	
	execution_node_needed(memory_manager, self, parent);
	return self;
}

ExecutionNode* ignore_new(Environment* memory_manager, ExecutionNode const* parent) {
	return execution_node_new(
		memory_manager,
		ignore, NULL, 0,
		parent
	);
}
