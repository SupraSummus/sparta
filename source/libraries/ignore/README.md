Ignore is a library providing node that ignores anything and returns self. It is useful for commenting code (assume that `@` is a map, and under `//` key ignore node is stored):

	@ get // Lol it is a comment, [blah] {wont be executed but has to be syntax-correct} ;
	@ get // beware of using semicolon - it will reset reset current node ;
	@ get // quotes are bad too ;
	@ get // "Inside quotes you can do anything ;;''\"\" and it's okay" ;
	@ get // Too bad that this comments are slowing execution down ;
	@ get // Quoted comments are faster because ingore node has to process only one argument ;
