#ifndef LIBSPARTA_IGNORE_H
#define LIBSPARTA_IGNORE_H

#include <sparta/execution_node.h>

/**
 * * -> <comment>
 */
extern ExecutionNodeHandler ignore;

extern ExecutionNode* ignore_new(Environment*, ExecutionNode const* parent);

#endif
