#include "map.h"
#include "../../utilities/debug.h"
#include "../../utilities/hash_map/hash_map.h"
#include "../../utilities/hash_map/hash_map_utilities.h"
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sparta/library.h>
#include <sparta/lib/boolean.h>

typedef struct {
	ExecutionNode* map;
	ExecutionNode* key;
} MapSetKeyData;

ExecutionNodeFinalizer map_finalizer;

/**
 * private
 */

void map_finalizer(Environment* memory_manager, ExecutionNode* map_node) {
	(void)memory_manager;
	HashMap* hash_map = execution_node_pointer_get_data(map_node);
	hash_map_free(hash_map);
}

/**
 * init
 */

ExecutionNode* initialize(Environment* memory_manager, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	(void)configuration;
	return map_factory_new(memory_manager, parent);
}

/**
 * public handlers
 */

ExecutionNode* map_factory(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)self;
	
	if (execution_node_is_symbol(argument, "new")) {
		return map_new(memory_manager, parent);
	}
	
	warn("map_factory received unexpected argument");
	return NULL;
}

ExecutionNode* map(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	if (execution_node_is_symbol(argument, "set")) {
		return execution_node_pointer_new(
			memory_manager,
			map_set, self, NULL,
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, "get")) {
		return execution_node_pointer_new(
			memory_manager,
			map_get, self, NULL,
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, "has")) {
		return execution_node_pointer_new(
			memory_manager,
			map_has, self, NULL,
			parent
		);
	}
	
	warn("map received unexpected argument");
	return NULL;
}

ExecutionNode* map_set(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	if (!execution_node_is_any_symbol(argument)) {
		warn("map_set expected symbol as a key");
		return NULL;
	}
	
	ExecutionNode* result = execution_node_new(
		memory_manager,
		map_set_key, NULL, sizeof(MapSetKeyData),
		parent
	);
	MapSetKeyData* map_set_key_data = execution_node_get_data(result);
	
	map_set_key_data->map = execution_node_pointer_get_data(self);
	map_set_key_data->key = argument;
	
	execution_node_needed(memory_manager, map_set_key_data->map, result);
	execution_node_needed(memory_manager, map_set_key_data->key, result);
	
	return result;
}

ExecutionNode* map_set_key(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	MapSetKeyData* map_data = execution_node_get_data(self);
	map_node_set(
		memory_manager,
		map_data->map,
		execution_node_get_symbol(map_data->key),
		argument
	);
	
	execution_node_needed(memory_manager, map_data->map, parent);
	return map_data->map;
}

ExecutionNode* map_get(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	if (!execution_node_is_any_symbol(argument)) {
		warn("map_get expected symbol as a key");
		return NULL;
	}
	
	ExecutionNode* map_node = execution_node_pointer_get_data(self);
	ExecutionNode* value = hash_map_get(execution_node_pointer_get_data(map_node), execution_node_get_symbol(argument));
	execution_node_needed(memory_manager, value, parent);
	return value;
}

ExecutionNode* map_has(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	if (!execution_node_is_any_symbol(argument)) {
		warn("map_has expected symbol as a key");
		return NULL;
	}
	
	ExecutionNode* map_node = execution_node_pointer_get_data(self);
	return boolean_new(
		memory_manager,
		hash_map_has(execution_node_pointer_get_data(map_node), execution_node_get_symbol(argument)),
		parent
	);
}

ExecutionNode* map_new(Environment* memory_manager, ExecutionNode const* parent) {
	// TODO store directly
	HashMap* hash_map = string_key_hash_map_new();
	ExecutionNode* map_node = execution_node_pointer_new(
		memory_manager,
		map, hash_map, map_finalizer,
		parent
	);
	return map_node;
}

ExecutionNode* map_factory_new(Environment* memory_manager, ExecutionNode const* parent) {
	return execution_node_new(
		memory_manager,
		map_factory, NULL, 0,
		parent
	);
}

void map_node_set(Environment* memory_manager, ExecutionNode* map_node, char const* key, ExecutionNode* value) {
	if (!execution_node_is(map_node, map)) {
		warn("map_node_set(): called on non-map node");
		return;
	}
	HashMap* hash_map = execution_node_pointer_get_data(map_node);
	ExecutionNode* old_value = hash_map_get(hash_map, key);
	if (old_value != NULL) {
		execution_node_not_needed(memory_manager, old_value, map_node);
	}
	execution_node_needed(memory_manager, value, map_node);
	hash_map_set(hash_map, key, value);
}
