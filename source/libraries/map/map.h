#ifndef LIBSPARTA_MAP_H
#define LIBSPARTA_MAP_H

#include <sparta/execution_node.h>

/**
 * new -> <map>
 */
extern ExecutionNodeHandler map_factory;

/**
 * set -> <map_set>
 * get -> <map_get>
 * has -> <map_has>
 */
extern ExecutionNodeHandler map;

/**
 * * -> <map_set_key>
 */
extern ExecutionNodeHandler map_set;

/**
 * *, <*> -> <>
 */
extern ExecutionNodeHandler map_set_key;

/**
 * * -> *, <*>
 */
extern ExecutionNodeHandler map_get;

/**
 * * -> <boolean>
 */
extern ExecutionNodeHandler map_has;

/**
 * utils
 */
extern ExecutionNode* map_new(Environment*, ExecutionNode const* parent);
extern ExecutionNode* map_factory_new(Environment*, ExecutionNode const* parent);
extern void map_node_set(Environment*, ExecutionNode* map, char const* key, ExecutionNode* value);

#endif
