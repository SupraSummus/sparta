#include "print.h"
#include <sparta/library.h>
#include <sparta/execution_node.h>
#include "../../utilities/debug.h"
#include <stdio.h>

ExecutionNode* initialize(Environment* memory_manager, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	(void)configuration;
	return print_new(memory_manager, parent);
}

ExecutionNode* print(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)memory_manager;
	(void)self;
	(void)parent;
	
	if (execution_node_is_any_symbol(argument)) {
		printf(execution_node_get_symbol(argument));
	} else {
		warn("print received non-symbol");
	}
	
	return NULL;
}

ExecutionNode* print_new(Environment* memory_manager, ExecutionNode const* parent) {
	return execution_node_new(
		memory_manager,
		print, NULL, 0,
		parent
	);
}
