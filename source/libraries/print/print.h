#ifndef LIBSPARTA_PRINT_H
#define LIBSPARTA_PRINT_H

#include <sparta/execution_node.h>

/**
 * * -> <>
 */
extern ExecutionNodeHandler print;

extern ExecutionNode* print_new(Environment*, ExecutionNode const* parent);

#endif
