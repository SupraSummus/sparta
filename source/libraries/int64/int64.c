#include "int64.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../utilities/debug.h"
#include <sparta/execution_node.h>
#include <sparta/library.h>
#include <sparta/lib/boolean.h>

/**
 * init
 */

ExecutionNode* initialize(Environment* memory_manager, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	(void)configuration;
	return int64_factory_new(memory_manager, parent);
}

/**
 * node handlers
 */

ExecutionNode* int64_factory(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)self;

	if (execution_node_is_any_symbol(argument)) {
		int64_t parsed;
		char some_char;
		if (sscanf(execution_node_get_symbol(argument), "%" SCNd64 "%c", &parsed, &some_char) != 1) {
			warn("int64_factory expected symbol parsable to integer");
			return NULL;
		}
		return int64_new(memory_manager, parsed, parent);
	}
	
	warn("int64_factory: expected: any symbol");
	return NULL;
}

ExecutionNode* int64(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {

	if (execution_node_is_symbol(argument, "symbol")) {
		char buffer[100];
		snprintf(buffer, 100, "%"SCNd64, int64_get_value(self));
		return execution_node_symbol_new(memory_manager, buffer, parent);
	}
	
	if (execution_node_is_symbol(argument, "+")) {
		return execution_node_pointer_new(
			memory_manager,
			int64_plus,
			self,
			NULL,
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, "-")) {
		return execution_node_pointer_new(
			memory_manager,
			int64_minus,
			self,
			NULL,
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, "%")) {
		return execution_node_pointer_new(
			memory_manager,
			int64_mod,
			self,
			NULL,
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, "/")) {
		return execution_node_pointer_new(
			memory_manager,
			int64_div,
			self,
			NULL,
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, "=")) {
		return execution_node_pointer_new(
			memory_manager,
			int64_equal,
			self,
			NULL,
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, "<")) {
		return execution_node_pointer_new(
			memory_manager,
			int64_less,
			self,
			NULL,
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, ">")) {
		return execution_node_pointer_new(
			memory_manager,
			int64_greater,
			self,
			NULL,
			parent
		);
	}
	
	warn("int64: expected: symbol, +, -, %, =, <, >");
	return NULL;
}

ExecutionNode* int64_plus(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {

	if (execution_node_is(argument, int64)) {
		return int64_new(memory_manager, int64_get_value(execution_node_pointer_get_data(self)) + int64_get_value(argument), parent);
	}
	
	warn("int64_plus: expected: node int64");
	return NULL;
}

ExecutionNode* int64_minus(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {

	if (execution_node_is(argument, int64)) {
		return int64_new(memory_manager, int64_get_value(execution_node_pointer_get_data(self)) - int64_get_value(argument), parent);
	}
	
	warn("int64_minus: expected: node int64");
	return NULL;
}

ExecutionNode* int64_mod(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {

	if (execution_node_is(argument, int64)) {
		return int64_new(memory_manager, int64_get_value(execution_node_pointer_get_data(self)) % int64_get_value(argument), parent);
	}
	
	warn("int64_mod: expected node int64");
	return NULL;
}

ExecutionNode* int64_div(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {

	if (execution_node_is(argument, int64)) {
		return int64_new(memory_manager, int64_get_value(execution_node_pointer_get_data(self)) / int64_get_value(argument), parent);
	}
	
	warn("int64_div: expected node int64");
	return NULL;
}

ExecutionNode* int64_equal(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {

	if (execution_node_is(argument, int64)) {
		return boolean_new(memory_manager, int64_get_value(execution_node_pointer_get_data(self)) == int64_get_value(argument), parent);
	}
	
	warn("int64_equal: expected: node int64");
	return NULL;
}

ExecutionNode* int64_less(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {

	if (execution_node_is(argument, int64)) {
		return boolean_new(memory_manager, int64_get_value(execution_node_pointer_get_data(self)) < int64_get_value(argument), parent);
	}
	
	warn("int64_less: expected: node int64");
	return NULL;
}

ExecutionNode* int64_greater(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {

	if (execution_node_is(argument, int64)) {
		return boolean_new(memory_manager, int64_get_value(execution_node_pointer_get_data(self)) > int64_get_value(argument), parent);
	}
	
	warn("int64_greater: expected: node int64");
	return NULL;
}

/**
 * utils
 */

ExecutionNode* int64_new(Environment* memory_manager, int64_t value, ExecutionNode const* parent) {
	ExecutionNode* node = execution_node_new(
		memory_manager,
		int64, NULL, sizeof(int64_t),
		parent
	);
	*(int64_t*)execution_node_get_data(node) = value;
	return node;
}

ExecutionNode* int64_factory_new(Environment* memory_manager, ExecutionNode const* parent) {
	return execution_node_new(
		memory_manager,
		int64_factory, NULL, 0,
		parent
	);
}

int64_t int64_get_value(ExecutionNode* node) {
	// TODO check if it's an int64 node?
	return *((int64_t*)execution_node_get_data(node));
}
