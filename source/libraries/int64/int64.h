#ifndef LIBSPARTA_INT64_H
#define LIBSPARTA_INT64_H

#include <stdint.h>
#include <sparta/execution_node.h>

extern ExecutionNodeHandler int64_factory;

extern ExecutionNodeHandler int64;

/**
 * maths
 */
extern ExecutionNodeHandler int64_plus;
extern ExecutionNodeHandler int64_minus;
extern ExecutionNodeHandler int64_mod;
extern ExecutionNodeHandler int64_div;

/**
 * comparing
 */
extern ExecutionNodeHandler int64_equal;
extern ExecutionNodeHandler int64_less;
extern ExecutionNodeHandler int64_greater;

/**
 * utils
 */
extern ExecutionNode* int64_new(Environment*, int64_t, ExecutionNode const* parent);
extern ExecutionNode* int64_factory_new(Environment*, ExecutionNode const* parent);
extern int64_t int64_get_value(ExecutionNode*);

#endif
