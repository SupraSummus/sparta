#include "memory_manager_interface.h"
#include "../../utilities/debug.h"
#include <sparta/library.h>
#include <sparta/lib/int64.h>

/**
 * initializer
 */

ExecutionNode* initialize(Environment* memory_manager, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	(void)configuration;
	return memory_manager_interface_new(memory_manager, parent);
}

/**
 * handlers
 */

ExecutionNode* memory_manager_interface(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)self;
	(void)parent;
	
	if (execution_node_is_symbol(argument, "info")) {
		memory_manager_print_debug(memory_manager);
		return NULL;
	}
	
	if (execution_node_is_symbol(argument, "size")) {
		return int64_new(
			memory_manager,
			memory_manager_get_size(memory_manager),
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, "full_clean")) {
		memory_manager_full_clean(memory_manager);
		return NULL;
	}
	
	return NULL;
}

/**
 * helpers
 */

ExecutionNode* memory_manager_interface_new(Environment* memory_manager, ExecutionNode const* parent) {
	return execution_node_new(
		memory_manager,
		memory_manager_interface, NULL, 0,
		parent
	);
}
