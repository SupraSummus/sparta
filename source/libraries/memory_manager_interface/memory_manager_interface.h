#ifndef LIBSPARTA_MEMORY_MANAGER_INTERFACE_H
#define LIBSPARTA_MEMORY_MANAGER_INTERFACE_H

#include <sparta/execution_node.h>

/**
 * info -> <>
 * size -> <int64>
 * full_clean -> <>
 */
extern ExecutionNodeHandler memory_manager_interface;

/**
 * helpers
 */
extern ExecutionNode* memory_manager_interface_new(Environment*, ExecutionNode const* parent);

#endif
