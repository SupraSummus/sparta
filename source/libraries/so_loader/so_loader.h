#ifndef LIBSPARTA_SO_LOADER_H
#define LIBSPARTA_SO_LOADER_H

#include <sparta/execution_node.h>
#include <sparta/library.h>

extern ExecutionNodeHandler so_loader;

/**
 * utils
 */
extern ExecutionNode* so_loader_new(Environment*, ExecutionNode const* parent);

#endif
