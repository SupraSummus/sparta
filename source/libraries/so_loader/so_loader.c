#include "so_loader.h"
#include "../../utilities/debug.h"
#include <sparta/execution_node.h>
#include <sparta/library.h>
#include <sparta/loader.h>

/**
 * initializer
 */

ExecutionNode* initialize(Environment* memory_manager, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	(void)configuration;
	return so_loader_new(memory_manager, parent);
}

/**
 * node handlers
 */

ExecutionNode* so_loader(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)self;

	if (!execution_node_is_any_symbol(argument)) {
		warn("so_loader expected symbol");
	}

	// TODO configuration
	return loader_load(memory_manager, execution_node_get_symbol(argument), NULL, parent);
}

/**
 * utils
 */

ExecutionNode* so_loader_new(Environment* memory_manager, ExecutionNode const* parent) {
	return execution_node_new(
		memory_manager,
		so_loader, NULL, 0,
		parent
	);
}
