# Makefile template for sparta shared library
#
# you have to set:
#  * LIBRARY_NAME - name of this library
#  * LIBRARY_DEPENDENCIES - set of library names needed
#  * LIBRARY_HEADER - header exported to include dir

ROOT = ../../../
LIBRARY_FILE_NAME = $(LIB_DIR)libsparta_$(LIBRARY_NAME).so
CFLAGS = -Wall -Wextra --pedantic -std=gnu99 -fPIC -I$(INCLUDE_DIR)
LDFLAGS = -shared -L$(LIB_DIR) $(foreach DEP,$(LIBRARY_DEPENDENCIES),-lsparta_$(DEP) )
TARGETS = $(LIBRARY_NAME).so
CAN_HAZ = $(OBJECTS) $(shell find $(ROOT)source/utilities -name '*.o')
MUST_HAZ =
CUSTOM_ALL = $(LIBRARY_FILE_NAME)

ifdef LIBRARY_HEADER
CUSTOM_ALL += $(SPARTA_LIB_INCLUDE_DIR)$(LIBRARY_NAME).h
endif

include $(ROOT)source/standard.mk

$(LIBRARY_FILE_NAME): $(TARGETS)
	cp $< $@

ifdef LIBRARY_HEADER
$(SPARTA_LIB_INCLUDE_DIR)$(LIBRARY_NAME).h: $(LIBRARY_HEADER)
	cp $< $@
endif
