#include "debug.h"
#include "../../utilities/debug.h"
#include <execinfo.h>
#include <stdlib.h>
#include <sparta/library.h>

/**
 * init
 */

ExecutionNode* initialize(Environment* memory_manager, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	(void)configuration;
	return debug_new(memory_manager, parent);
}

/**
 * handlers
 */

ExecutionNode* debug(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)self;
	(void)parent;
	(void)memory_manager;
	
	if (argument == NULL) {
		info("debug: received NULL node");
	} else if (execution_node_is_any_symbol(argument)) {
		info("debug: received symbol: %s", execution_node_get_symbol(argument));
	} else {
		void* handler_array[1];
		#pragma GCC diagnostic ignored "-Wpedantic"
		handler_array[0] = execution_node_get_handler(argument);
		#pragma GCC diagnostic pop
		
		char** handler_name_array;
		handler_name_array = backtrace_symbols(handler_array, 1);
		
		info(
			"debug: received node: handler %s, data %p",
			handler_name_array[0],
			execution_node_get_data(argument)
		);
		free(handler_name_array);
	}
	
	return NULL;
}

ExecutionNode* debug_new(Environment* memory_manager, ExecutionNode const* parent) {
	return execution_node_new(
		memory_manager,
		debug, NULL, 0,
		parent
	);
}
