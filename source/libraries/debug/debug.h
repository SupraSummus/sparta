#ifndef LIBSPARTA_DEBUG_H
#define LIBSPARTA_DEBUG_H

#include <sparta/execution_node.h>

/**
 * *, <*> -> <>
 */
extern ExecutionNodeHandler debug;

extern ExecutionNode* debug_new(Environment*, ExecutionNode const* parent);

#endif
