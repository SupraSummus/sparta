#include "eval.h"
#include "../../utilities/debug.h"
#include <sparta/execution_node.h>
#include <sparta/library.h>
#include <sparta/parser.h>
#include <sparta/executor.h>

/**
 * initializer
 */

ExecutionNode* initialize(Environment* environment, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	(void)configuration;
	return eval_new(environment, parent);
}

/**
 * node handlers
 */

ExecutionNode* eval(Environment* environment, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)self;

	if (!execution_node_is_any_symbol(argument)) {
		warn("eval expected symbol");
	}

	return execution_node_pointer_new(
		environment,
		eval_symbol, argument, NULL,
		parent
	);
}

ExecutionNode* eval_symbol(Environment* environment, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	char const* string = execution_node_get_symbol(execution_node_pointer_get_data(self));
	Vector* forest = parse(environment, string, NULL, NULL);
	ExecutionNode* node = execution_node_block_new(
		environment,
		forest, argument,
		parent
	);
	memory_manager_not_needed(environment, forest, NULL);
	return node;
}

/**
 * utils
 */

ExecutionNode* eval_new(Environment* environment, ExecutionNode const* parent) {
	return execution_node_new(
		environment,
		eval, NULL, 0,
		parent
	);
}
