#ifndef LIBSPARTA_EVAL_H
#define LIBSPARTA_EVAL_H

#include <sparta/execution_node.h>
#include <sparta/library.h>

/**
 * * -> <eval_symbol>
 */
extern ExecutionNodeHandler eval;

/**
 * *, <*> -> *, <*>
 */
extern ExecutionNodeHandler eval_symbol;

/**
 * utils
 * =====
 */
extern ExecutionNode* eval_new(Environment*, ExecutionNode const* parent);

#endif
