#define _GNU_SOURCE

#include "equal.h"
#include "../../utilities/debug.h"
#include <sparta/lib/boolean.h>
#include <string.h>

/**
 * initializer
 */

ExecutionNode* initialize(Environment* memory_manager, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	(void)configuration;
	return equal_new(memory_manager, parent);
}

/**
 * handlers
 */

ExecutionNode* equal(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)self;
	return execution_node_pointer_new(memory_manager, equal_node, argument, NULL, parent);
}

ExecutionNode* equal_node(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	ExecutionNode* node = execution_node_pointer_get_data(self);
	return boolean_new(
		memory_manager,
		equal_nodes(node, argument),
		parent
	);
}

/**
 * helpers
 */

bool equal_nodes(ExecutionNode const* a, ExecutionNode const* b) {
	if (a == b) {
		return true;
	}
	
	if ( /* special case for symbols */
		execution_node_is_any_symbol(a) &&
		execution_node_is_any_symbol(b)
	) {
		return strcmp(execution_node_get_symbol(a), execution_node_get_symbol(b)) == 0;
	}
	
	return false;
}

ExecutionNode* equal_new(Environment* memory_manager, ExecutionNode const* parent) {
	return execution_node_new(
		memory_manager,
		equal, NULL, 0,
		parent
	);
}
