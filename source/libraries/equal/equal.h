#ifndef LIBSPARTA_EQUAL_H
#define LIBSPARTA_EQUAL_H

#include <sparta/library.h>
#include <stdbool.h>

/**
 * *, <*> -> <equal_node>
 */
extern ExecutionNodeHandler equal;

/**
 * *, <*> -> <boolean>
 */
extern ExecutionNodeHandler equal_node;

/**
 * helpers
 */
extern bool equal_nodes(ExecutionNode const*, ExecutionNode const*);
extern ExecutionNode* equal_new(Environment*, ExecutionNode const* parent);

#endif
