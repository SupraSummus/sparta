#ifndef LIBSPARTA_TYPE_H
#define LIBSPARTA_TYPE_H

#include <sparta/execution_node.h>

/**
 * * -> <type>
 */
extern ExecutionNodeHandler type_factory;

/**
 * = -> <type_equal>
 * info -> * (symbol)
 */
extern ExecutionNodeHandler type;

/**
 * <type> -> <boolean>
 */
extern ExecutionNodeHandler type_equal;

/**
 * helpers
 */
extern ExecutionNode* type_factory_new(Environment*, ExecutionNode const* parent);

#endif
