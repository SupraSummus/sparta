#define _GNU_SOURCE

#include "type.h"
#include "../../utilities/debug.h"
#include <dlfcn.h>
#include <execinfo.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <sparta/library.h>
#include <sparta/lib/boolean.h>

bool type_is_the_same(ExecutionNode const*, ExecutionNode const*);

/**
 * private
 */

bool type_is_the_same(ExecutionNode const* a, ExecutionNode const* b) {
	if (a == b) {
		return true;
	}
	
	if (a == NULL || b == NULL) {
		return false;
	}
	
	return execution_node_get_handler(a) == execution_node_get_handler(b);
}

/**
 * initializer
 */

ExecutionNode* initialize(Environment* memory_manager, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	(void)configuration;
	return type_factory_new(memory_manager, parent);
}

/**
 * handlers
 */

ExecutionNode* type_factory(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)self;
	return execution_node_pointer_new(memory_manager, type, argument, NULL, parent);
}

ExecutionNode* type(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	if (execution_node_is_symbol(argument, "=")) {
		return execution_node_pointer_new(memory_manager, type_equal, execution_node_pointer_get_data(self), NULL, parent);
	}
	
	if (execution_node_is_symbol(argument, "info")) {
		/* info based on backtrace_symbols() function */
		ExecutionNode* self_data = execution_node_pointer_get_data(self);
		
		if (self_data == NULL) {
			return execution_node_symbol_new(memory_manager, "NULL", parent);
		} else if (execution_node_is_any_symbol(self_data)) {
			return execution_node_symbol_new(memory_manager, "symbol", parent);
		} else {
			void* handler_array[1];
			#pragma GCC diagnostic ignored "-Wpedantic"
			handler_array[0] = execution_node_get_handler(self_data);
			#pragma GCC diagnostic pop
			
			char** handler_name_array = backtrace_symbols(handler_array, 1);
			ExecutionNode* result = execution_node_symbol_new(memory_manager, handler_name_array[0], parent);
			free(handler_name_array);
			
			return result;
		}
		
	}
	
	if (execution_node_is_symbol(argument, "handler_name")) {
		/* based on dladdr() function */
		ExecutionNode* self_data = execution_node_pointer_get_data(self);
		
		if (self_data == NULL) {
			return execution_node_symbol_new(memory_manager, "(NULL)", parent);
		} else if (execution_node_is_any_symbol(self_data)) {
			return execution_node_symbol_new(memory_manager, "(symbol)", parent);
		} else {
			Dl_info info;
			#pragma GCC diagnostic ignored "-Wpedantic"
			if (dladdr(execution_node_get_handler(self_data), &info) == 0) {
				#pragma GCC diagnostic pop
				warn("type: dladdr error");
				return NULL;
			}
			
			return execution_node_symbol_new(memory_manager, info.dli_sname, parent);
		}
		
	}
	
	return NULL;
}

ExecutionNode* type_equal(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	
	if (execution_node_is(argument, type)) {
		return boolean_new(
			memory_manager,
			type_is_the_same(execution_node_pointer_get_data(self), execution_node_pointer_get_data(argument)),
			parent
		);
	}
	
	warn("type_equal expected type node");
	return NULL;
}

/**
 * helpers
 */

ExecutionNode* type_factory_new(Environment* memory_manager, ExecutionNode const* parent) {
	return execution_node_new(
		memory_manager,
		type_factory, NULL, 0,
		parent
	);
}
