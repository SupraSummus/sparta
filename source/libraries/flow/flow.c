#include "flow.h"
#include "../../utilities/debug.h"
#include <stdlib.h>
#include <sparta/library.h>
#include <sparta/lib/boolean.h>

/**
 * init
 */

ExecutionNode* initialize(Environment* memory_manager, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	(void)configuration;
	return flow_new(memory_manager, parent);
}

/**
 * handlers
 */

ExecutionNode* flow(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)self;
	
	if (execution_node_is_symbol(argument, "if")) {
		return execution_node_new(
			memory_manager,
			flow_if, NULL, 0,
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, "while")) {
		return execution_node_new(
			memory_manager,
			flow_while, NULL, 0,
			parent
		);
	}
	
	warn("flow received unexpected argument");
	return NULL;
}

ExecutionNode* flow_if(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)self;
	
	if (execution_node_is(argument, boolean)) {
		return execution_node_pointer_new(
			memory_manager,
			flow_if_condition, argument, NULL,
			parent
		);
	}
	
	warn("flow_if received unexpected argument");
	return NULL;
}

ExecutionNode* flow_if_condition(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)parent;
	
	if (!execution_node_is_any_symbol(argument)) {
		if (boolean_get_value(execution_node_pointer_get_data(self))) {
			ExecutionNode* result = execution_node_call(memory_manager, argument, NULL, NULL);
			if (result != NULL) {
				execution_node_not_needed(memory_manager, result, NULL);
				result = NULL;
			}
		}
		return NULL;
	}
	
	warn("flow_if_condition received unexpected argument");
	return NULL;
}

ExecutionNode* flow_while(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)self;
	
	if (!execution_node_is_any_symbol(argument)) {
		return execution_node_pointer_new(
			memory_manager,
			flow_while_condition, argument, NULL,
			parent
		);
	}
	
	warn("flow_while received unexpected argument");
	return NULL;
}

ExecutionNode* flow_while_condition(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)parent;
	
	if (!execution_node_is_any_symbol(argument)) {
		while (true) {
			ExecutionNode* condition = execution_node_call(memory_manager, execution_node_pointer_get_data(self), NULL, NULL);
			bool condition_bool;
			if (!execution_node_is(condition, boolean)) {
				warn("flow_while_condition: test function returned non-bool value");
				condition_bool = false;
			} else {
				condition_bool = boolean_get_value(condition);
			}
			if (condition != NULL) {
				execution_node_not_needed(memory_manager, condition, NULL);
				condition = NULL;
			}
			
			if (!condition_bool) {
				break;
			}
			
			ExecutionNode* result = execution_node_call(memory_manager, argument, NULL, NULL);
			if (result != NULL) {
				execution_node_not_needed(memory_manager, result, NULL);
				result = NULL;
			}
		}
		
		return NULL;
	}
	
	warn("flow_while_condition received unexpected argument");
	return NULL;
}

ExecutionNode* flow_new(Environment* memory_manager, ExecutionNode const* parent) {
	return execution_node_new(
		memory_manager,
		flow, NULL, 0,
		parent
	);
}
