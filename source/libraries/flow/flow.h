#ifndef LIBSPARTA_FLOW_H
#define LIBSPARTA_FLOW_H

#include <sparta/execution_node.h>

/**
 * if -> <flow_if>
 * while -> <flow_while>
 */
extern ExecutionNodeHandler flow;

/**
 * <boolean> -> <flow_if_condition>
 */
extern ExecutionNodeHandler flow_if;

/**
 * <*> -> <>
 */
extern ExecutionNodeHandler flow_if_condition;

/**
 * <<> -> <boolean>> -> <flow_while_condition>
 */
extern ExecutionNodeHandler flow_while;

/**
 * <*> -> <>
 */
extern ExecutionNodeHandler flow_while_condition;

extern ExecutionNode* flow_new(Environment*, ExecutionNode const* parent);

#endif
