#include <sparta/library.h>
#include <sparta/loader.h>
#include <sparta/lib/map.h>
#include "../../utilities/debug.h"

ExecutionNode* initialize(Environment* memory_manager, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	ExecutionNode* map_node = map_new(memory_manager, parent);
	
	if (configuration == NULL) {
		warn("custom: initialize: expected nonNULL configuration");
		return map_node;
	}
	
	while (configuration->string_count >= 2) {
		char* key = configuration->strings[0];
		configuration->string_count --;
		configuration->strings ++;
		
		char* module_file_name = configuration->strings[0];
		configuration->string_count --;
		configuration->strings ++;
		
		ExecutionNode* module_node = loader_load(
			memory_manager,
			module_file_name, configuration,
			NULL
		);
		
		map_node_set(memory_manager, map_node, key, module_node);
		execution_node_not_needed(memory_manager, module_node, NULL);
	}
	
	return map_node;
}
