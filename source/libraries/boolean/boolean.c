#include "boolean.h"
#include "../../utilities/debug.h"
#include <sparta/execution_node.h>
#include <sparta/library.h>
#include <stdbool.h>
#include <stdlib.h>

/**
 * initializer
 */

ExecutionNode* initialize(Environment* memory_manager, LibraryInitializerConfiguration* configuration, ExecutionNode const* parent) {
	(void)configuration;
	return boolean_factory_new(memory_manager, parent);
}

/**
 * node handler
 */

ExecutionNode* boolean_factory(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	(void)self;
	
	if (execution_node_is_symbol(argument, "true")) {
		return boolean_new(memory_manager, true, parent);
	}
	
	if (execution_node_is_symbol(argument, "false")) {
		return boolean_new(memory_manager, false, parent);
	}
	
	warn("boolean_factory received unexpected argument");
	return NULL;
}

ExecutionNode* boolean(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	if (execution_node_is_symbol(argument, "not")) {
		return boolean_new(
			memory_manager,
			!boolean_get_value(self),
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, "copy")) {
		return boolean_new(
			memory_manager,
			boolean_get_value(self),
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, "and")) {
		return execution_node_pointer_new(
			memory_manager,
			boolean_and, self, NULL,
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, "or")) {
		return execution_node_pointer_new(
			memory_manager,
			boolean_or, self, NULL,
			parent
		);
	}
	
	if (
		execution_node_is_symbol(argument, "=") ||
		execution_node_is_symbol(argument, "xnor")
	) {
		return execution_node_pointer_new(
			memory_manager,
			boolean_equal, self, NULL,
			parent
		);
	}
	
	if (execution_node_is_symbol(argument, "symbol")) {
		return execution_node_symbol_new(
			memory_manager,
			boolean_get_value(self) ? "true" : "false",
			parent
		);
	}
	
	warn("boolean received unexpected argument");
	return NULL;
}

ExecutionNode* boolean_and(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	if (execution_node_is(argument, boolean)) {
		return boolean_new(
			memory_manager,
			boolean_get_value(execution_node_pointer_get_data(self)) && boolean_get_value(argument),
			parent
		);
	}
	
	warn("boolean_and received unexpected argument");
	return NULL;
}

ExecutionNode* boolean_or(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	if (execution_node_is(argument, boolean)) {
		return boolean_new(
			memory_manager,
			boolean_get_value(execution_node_pointer_get_data(self)) || boolean_get_value(argument),
			parent
		);
	}
	
	warn("boolean_or received unexpected argument");
	return NULL;
}

ExecutionNode* boolean_equal(Environment* memory_manager, ExecutionNode* self, ExecutionNode* argument, ExecutionNode const* parent) {
	if (execution_node_is(argument, boolean)) {
		return boolean_new(
			memory_manager,
			boolean_get_value(execution_node_pointer_get_data(self)) == boolean_get_value(argument),
			parent
		);
	}
	
	warn("boolean_equal received unexpected argument");
	return NULL;
}

bool boolean_get_value(ExecutionNode* node) {
	if (!execution_node_is(node, boolean)) {
		erro("boolean_get_value called with non-boolean");
		return false;
	}
	return *((bool*)execution_node_get_data(node));
}

ExecutionNode* boolean_new(Environment* memory_manager, bool value, ExecutionNode const* parent) {
	ExecutionNode* node = execution_node_new(
		memory_manager,
		boolean, NULL, sizeof(bool),
		parent
	);
	*(bool*)execution_node_get_data(node) = value;
	return node;
}

ExecutionNode* boolean_factory_new(Environment* memory_manager, ExecutionNode const* parent) {
	return execution_node_new(
		memory_manager,
		boolean_factory, NULL, 0,
		parent
	);
}
