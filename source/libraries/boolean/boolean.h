#ifndef LIBSPARTA_BOOLEAN_H
#define LIBSPARTA_BOOLEAN_H

#include <sparta/execution_node.h>
#include <stdbool.h>

/**
 * true -> <boolean>
 * false -> <boolean>
 */
extern ExecutionNodeHandler boolean_factory;

/**
 * not -> <boolean>
 * or -> <boolean_or>
 * and -> <boolean_and>
 * =, xnor -> < <boolean_equal>
 * copy -> <boolean>
 * symbol -> true, flase
 */
extern ExecutionNodeHandler boolean;

/**
 * <boolean> -> <boolean>
 */
extern ExecutionNodeHandler boolean_and;

/**
 * <boolean> -> <boolean>
 */
extern ExecutionNodeHandler boolean_or;

/**
 * <boolean> -> <boolean>
 */
extern ExecutionNodeHandler boolean_equal;

/**
 * helpers
 */
extern bool boolean_get_value(ExecutionNode*);
extern ExecutionNode* boolean_new(Environment*, bool, ExecutionNode const* parent);
extern ExecutionNode* boolean_factory_new(Environment*, ExecutionNode const* parent);

#endif
