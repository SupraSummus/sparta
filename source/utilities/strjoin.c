#include "strjoin.h"
#include <string.h>
#include <stdlib.h>

char* strjoin(Vector* strings) {
	/* count length */
	size_t total_size = 0;
	for (size_t i = 0; i < vector_size(strings); i ++) {
		total_size += strlen(vector_get(strings, i));
	}
	
	/* concatenate */
	char* result = malloc(total_size + 1);
	*result = '\0';
	for (size_t i = 0; i < vector_size(strings); i ++) {
		strcat(result, vector_get(strings, i));
	}
	
	/* free */
	for (size_t i = 0; i < vector_size(strings); i ++) {
		free(vector_get(strings, i));
	}
	vector_free(strings);
	
	return result;
}
