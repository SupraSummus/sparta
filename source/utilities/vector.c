#include "vector.h"
#include <stdlib.h>

struct _Vector {
	VectorMallocFunction* malloc_function;
	VectorFreeFunction* free_function;
	void* custom_data;
	
	/**
	 * number of elements stored
	 */
	size_t length;
	
	/**
	 * size of allocated array
	 */
	size_t size;
	
	/**
	 * array of elements
	 */
	void** elements;
};

static VectorMallocFunction _malloc_function;
static VectorFreeFunction _free_function;

size_t vector_sizeof() {
	return sizeof(Vector);
}

Vector* vector_new(
	VectorMallocFunction* malloc_function,
	VectorFreeFunction* free_function,
	void* custom_data
){
	Vector* new = malloc_function(sizeof(Vector), custom_data);
	vector_initialize(
		new,
		malloc_function,
		free_function,
		custom_data
	);
	return new;
}

void vector_initialize(
	Vector* new,
	VectorMallocFunction* malloc_function,
	VectorFreeFunction* free_function,
	void* custom_data
) {
	new->malloc_function = malloc_function;
	new->free_function = free_function;
	new->custom_data = custom_data;
	new->length = 0;
	new->size = 1;
	new->elements = new->malloc_function(new->size * sizeof(void*), new->custom_data);
}

void vector_deinitialize(Vector* v) {
	v->free_function(v->elements, v->custom_data);
}

void vector_free(Vector* v){
	vector_deinitialize(v);
	v->free_function(v, v->custom_data);
}

void vector_push(Vector* v, void* e){
	/* grow vector */
	if (v->length == v->size) {
		size_t original_size = v->size;
		v->size *= 2;
		
		/* alloc new */
		void** new_elements = v->malloc_function(v->size * sizeof(void*), v->custom_data);
		
		/* rewrite */
		for (size_t i = 0; i < original_size; i ++) {
			new_elements[i] = v->elements[i];
		}
		
		/* free old */
		v->free_function(v->elements, v->custom_data);
		
		v->elements = new_elements;
	}
	
	/* set element */
	v->elements[v->length] = e;
	v->length++;
}

// TODO implement vector_pop
//extern void* vector_pop(Vector*);

void* vector_get(Vector const* v, size_t n) {
	if (n >= v->length) {
		return NULL;
	}
	return v->elements[n];
}

size_t vector_size(Vector const* v) {
	return v->length;
}

Vector* vector_new_standard() {
	return vector_new(
		_malloc_function,
		_free_function,
		NULL
	);
}

void vector_initialize_standard(Vector* v) {
	vector_initialize(
		v,
		_malloc_function,
		_free_function,
		NULL
	);
}

void* vector_top(Vector* v) {
	if (v->length == 0) {
		return NULL;
	}
	return vector_get(v, v->length - 1);
}

/**
 * private
 */
static void* _malloc_function(size_t size, void* null) {
	(void)null;
	return malloc(size);
}

static void _free_function(void* ptr, void* null) {
	(void)null;
	free(ptr);
}
