#ifndef STRJOIN_H
#define STRJOIN_H

#include "vector.h"

/**
 * Joins vector of strings into one string
 * Frees vector and strings stored in it
 * Returned string has to be freed() after use
 */
char* strjoin(Vector*);

#endif
