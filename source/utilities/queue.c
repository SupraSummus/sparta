#include "queue.h"

typedef struct _Entry Entry;
struct _Entry {
	Entry* next;
	void* data;
};

struct _Queue {
	QueueMallocFunction* malloc_function;
	QueueFreeFunction* free_function;
	
	Entry* first;
	Entry* last;
};

Queue* queue_new(QueueMallocFunction* malloc_function, QueueFreeFunction* free_function) {
	Queue* queue = malloc_function(sizeof(Queue));
	queue->malloc_function = malloc_function;
	queue->free_function = free_function;
	queue->first = NULL;
	queue->last = NULL;
	return queue;
}

void queue_free(Queue* queue) {
	while (!queue_is_empty(queue)) {
		(void)queue_get(queue);
	}
	queue->free_function(queue);
}

void queue_push(Queue* queue, void* data) {
	Entry* entry = queue->malloc_function(sizeof(Entry));
	entry->next = NULL;
	entry->data = data;
	
	if (queue->last == NULL) {
		/* queue was empty */
		queue->first = entry;
		queue->last = entry;
	} else {
		queue->last->next = entry;
		queue->last = entry;
	}
}

/**
 * if queue is emty returns null
 */
void* queue_get(Queue* queue) {
	if (queue->first == NULL) {
		/* queue empty */
		return NULL;
	} else {
		Entry* entry = queue->first;
		queue->first = queue->first->next;
		if (queue->first == NULL) {
			/* after get queue is empty */
			queue->last = NULL;
		}
		void* data = entry->data;
		queue->free_function(entry);
		return data;
	}
}

bool queue_is_empty(Queue* queue) {
	return queue->first == NULL;
}
