#ifndef VECTOR_H
#define VECTOR_H

#include <stddef.h>

typedef struct _Vector Vector;

typedef void* (VectorMallocFunction)(size_t, void* custom_data);
typedef void (VectorFreeFunction)(void*, void* custom_data);

extern size_t vector_sizeof();
extern Vector* vector_new(
	VectorMallocFunction*,
	VectorFreeFunction*,
	void* custom_data
);
extern void vector_initialize(
	Vector*,
	VectorMallocFunction*,
	VectorFreeFunction*,
	void* custom_data
);
extern void vector_deinitialize(Vector*);
extern void vector_free(Vector*);

extern void vector_push(Vector*, void*);
extern void* vector_pop(Vector*);

extern void* vector_get(Vector const*, size_t);

extern size_t vector_size(Vector const*);

/**
 * shortcuts
 * =========
 */
extern Vector* vector_new_standard();
extern void vector_initialize_standard(Vector*);
extern void* vector_top(Vector*);

#endif
