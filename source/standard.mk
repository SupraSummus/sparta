# Makefile template
#
# you have to set:
#  * ROOT - path to project main directory
#  * CFLAGS
#  * LDFLAGS
#  * TARGETS - names of targets
#  * (optional) MUST_HAZ - object files that has to be linked into targets
#  * CAN_HAZ - object files that can be linked into targets
#  * (optional) CUSTOM_ALL - name of tasks to be executed when 'all' task is called
#  * (optional) BEFORE_OBJECT_MAKEFILES
#  * (optional) CUSTOM_CLEAN - name of tasks to be executed when 'clean' task is called
#  * (optional) SOURCES - .c files to be compiled (defults to all *.c files)

CC = gcc
OBJECT_MAKEFILES = $(subst .c,.o.d,$(SOURCES))
OBJECTS = $(subst .c,.o,$(SOURCES))
TARGET_MAKEFILES = $(addsuffix .out.d,$(TARGETS))

SOURCES ?= $(wildcard *.c)

include $(ROOT)variables.mk

.SUFFIXES:

all: $(OBJECT_MAKEFILES) $(OBJECTS) $(TARGET_MAKEFILES) $(TARGETS) $(CUSTOM_ALL)

clean: $(CUSTOM_CLEAN)
	rm -rf $(OBJECT_MAKEFILES) $(OBJECTS) $(TARGET_MAKEFILES) $(TARGETS)

$(OBJECT_MAKEFILES) : %.o.d : %.c $(BEFORE_OBJECT_MAKEFILES)
	$(CC) -I$(INCLUDE_DIR) -M      $<  > $@
	echo -e "\t"$(CC) $(CFLAGS) -c $< >> $@

$(TARGET_MAKEFILES) : %.out.d : $(MUST_HAZ) $(CAN_HAZ)
	echo -n $(subst .out.d,,$@)": "                                                                              > $@
	$(ROOT)utilities/linking_dependencies.sh "$(addsuffix .o,$(basename $(subst .out.d,,$@))) $(MUST_HAZ)" "$^" >> $@
	echo -e "\t"$(CC)" "$(LDFLAGS)' -o $$@ $$^'                                                                 >> $@

ifneq ($(MAKECMDGOALS),clean)
-include $(OBJECT_MAKEFILES)
-include $(TARGET_MAKEFILES)
endif

