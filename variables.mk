# required variables:
#  * ROOT - project root directory

BUILD_DIR = $(ROOT)build/
LIB_DIR = $(BUILD_DIR)lib/
LIB_FILE = $(LIB_DIR)libsparta_$1.so
BIN_DIR = $(BUILD_DIR)bin/
SPARTA_INTERPRETER = LD_LIBRARY_PATH=$$LD_LIBRARY_PATH:$(LIB_DIR) $(BIN_DIR)sparta
INCLUDE_DIR = $(BUILD_DIR)include/
SPARTA_INCLUDE_DIR = $(INCLUDE_DIR)sparta/
SPARTA_LIB_INCLUDE_DIR = $(SPARTA_INCLUDE_DIR)lib/
