ROOT = ../../../
MODULE = $(call LIB_FILE,custom) \
	print $(call LIB_FILE,print) \
	map $(call LIB_FILE,map) \
	equal $(call LIB_FILE,equal)
TEST_NAMES = equal

include $(ROOT)tests/standard.mk
