ROOT = ../../../
MODULE = $(call LIB_FILE,custom) \
	map $(call LIB_FILE,map) \
	flow $(call LIB_FILE,flow) \
	print $(call LIB_FILE,print) \
	int64 $(call LIB_FILE,int64)
TEST_NAMES = gcd count

include $(ROOT)tests/standard.mk
	
