ROOT = ../../../
MODULE = $(call LIB_FILE,custom) \
	boolean $(call LIB_FILE,boolean) \
	map $(call LIB_FILE,map) \
	flow $(call LIB_FILE,flow) \
	print $(call LIB_FILE,print)
TEST_NAMES = custom

include $(ROOT)tests/standard.mk
	
