# makefile template for testing
#
# parameters:
#  * ROOT - project root directory
#  * (optional) MODULE - root module for sparta intepreter (defaults to so_loader)
#  * TEST_NAMES
#  * (optional) DEBUG - yes to show stdout and stderr

SHELL = bash

MODULE ?= $(call LIB_FILE,so_loader)
ALL_TESTS = $(addsuffix .test,$(TEST_NAMES))

include $(ROOT)variables.mk

.SUFFIXES:

test: $(ALL_TESTS)
	
$(ALL_TESTS): %:
	@START=`date +%s%N`; \
	echo -n "running "$@": "; \
	diff <($(SPARTA_INTERPRETER) $(MODULE) $@.sparta 2> /dev/null) $@.out; \
	END=`date +%s%N`; \
	if [ $$? -eq 0 ]; then \
		echo -e -n "\t\e[92mOK\e[0m \t"; \
	else \
		echo -e -n "\t\e[91mFAIL\e[0m \t"; \
	fi; \
	echo $$((($$END - $$START) / 1000 / 1000))"ms"
ifeq ($(DEBUG),yes)
	$(SPARTA_INTERPRETER) $(MODULE) $@.sparta
endif
